<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'middleware' => 'auth:api'], function () {

    /*
    |-------------------------------------------------------------------------------
    | Get User
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/user
    | Controller:     API\UsersController@getUser
    | Method:         GET
    | Description:    Gets the authenticated user
    */
    Route::get('/user', 'API\UsersController@getUser');

    /*
      |-------------------------------------------------------------------------------
      | Get All Objects
      |-------------------------------------------------------------------------------
      | URL:            /api/v1/objects
      | Controller:     API\ObjectsController@getObjects
      | Method:         GET
      | Description:    Gets all of the objects in the application
      */
    Route::get('/objects', 'API\ObjectsController@getObjects');

    /*
      |-------------------------------------------------------------------------------
      | Get All Regions
      |-------------------------------------------------------------------------------
      | URL:            /api/v1/objects
      | Controller:     API\RegionsController@getRegions
      | Method:         GET
      | Description:    Gets all of the regions in the application
      */
    Route::get('/regions', 'API\RegionsController@getRegions');

    /*
    |-------------------------------------------------------------------------------
    | Get An Individual Object
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/objects/{id}
    | Controller:     API\ObjectsController@getObject
    | Method:         GET
    | Description:    Gets an individual object
    */
    Route::get('/objects/{id}', 'API\ObjectsController@getObject');

});