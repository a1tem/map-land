<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/test', function (\App\Http\Api\GisFileApi $gisFileApi) {
//    $objects = \App\Models\Object::whereNull('category_id')->get();
//    foreach ($objects as $object) {
//        //$info = $gisFileApi->getInformationByCode($object->number);
//        $categoryId = \App\Utilities\ProcessCsvFile::getCategoryId($object->category);
//        $object->area = str_replace(',','.',$object->area);
//        if (!empty($categoryId)) {
//            $object->category_id = $categoryId;
//        }
////        $result = $gisFileApi->getCoordinatesByCode(trim($object->number));
////
////        if(!empty($result)) {
////            $object->lat = $result['latitude'];
////            $object->lng = $result['longitude'];
////            $object->save();
////        }
//        $object->save();
//    }
//});

Route::get('/', 'Web\AppController@getApp')->middleware('auth');

Route::get('/login', 'Web\AppController@getLogin' )
    ->name('login')
    ->middleware('guest');

Route::get('/policy', 'Web\AppController@getPrivacy' )
    ->name('privacy')
    ->middleware('guest');

Route::get( '/login/{social}', 'Web\AuthenticationController@getSocialRedirect' )
    ->middleware('guest');

Route::get( '/login/{social}/callback', 'Web\AuthenticationController@getSocialCallback' )
    ->middleware('guest');