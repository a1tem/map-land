<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-29492806-9"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-29492806-9');
    </script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta property="og:url"                content="https://mapland.com.ua" />
    <meta property="og:type"               content="website" />
    <meta property="og:title"              content="MapLand - Пошук земельних ділянок" />
    <meta property="og:description"        content="Пошук земельних ділянок на мапі" />
    <meta property="og:image"              content="https://mapland.com.ua/img/background-facebook.jpg" />
    <meta property="fb:app_id"             content="548420615504726"/>
    <link href="/css/app.css" rel="stylesheet" type="text/css"/>
    <link href="/css/fontello.css" rel="stylesheet" type="text/css"/>
    <link href="/css/fontello-codes.css" rel="stylesheet" type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
    <link rel="icon" type="image/x-icon" href="/img/icon.png">
    <title>MapLand - Пошук земельних ділянок</title>
    <script type='text/javascript'>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token()
        ]) !!}
    </script>
</head>
<body>

<div id="app">
    <router-view></router-view>
</div>

<script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_JS_API_KEY')}}"></script>
<script type="text/javascript" src="{{mix('js/app.js')}}"></script>
</body>
</html>