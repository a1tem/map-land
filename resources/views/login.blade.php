<!DOCTYPE html>
<html>
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-29492806-9"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-29492806-9');
    </script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:url"                content="https://mapland.com.ua" />
    <meta property="og:type"               content="website" />
    <meta property="og:title"              content="MapLand - Пошук земельних ділянок" />
    <meta property="og:description"        content="Пошук земельних ділянок на мапі" />
    <meta property="og:image"              content="https://mapland.com.ua/img/background-facebook.jpg" />
    <meta property="fb:app_id"             content="548420615504726"/>
    <title>MapLand - Пошук земельних ділянок</title>
    <link href="/css/app.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div id="login-page" class="bg-login">
    <div class="layout">
        <div class="box">
            <h1 class="no-margin">Sign in</h1>
            <ul class="social social-login">
                <li><a id="login-google" href="login/google" class="google flaticon-googleplus btn">
                        <span>Google</span></a>
                </li>
                <li>
                    <a id="login-facebook" href="login/facebook" class="facebook flaticon-facebook btn">
                        <span>Facebook</span>
                    </a>
                </li>
                <li>
                    <a id="login-twitter" href="login/twitter" class="twitter flaticon-twitter btn">
                        <span>Twitter</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
</body>
</html>