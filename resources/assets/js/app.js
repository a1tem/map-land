window._ = require('lodash');

try {
    window.$ = window.jQuery = require('jquery');
} catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

import Vue from 'vue';
import vSelect from 'vue-select'
import router from './routes.js';
import {store} from './store.js';

// load vuex i18n module
import vuexI18n from 'vuex-i18n';
import {RU} from './lang/ru';
import {UA} from './lang/ua';

/*
    Initializes Vuex on Vue.
*/
Vue.use(vuexI18n.plugin, store, {
    moduleName: 'i18n',
    onTranslationNotFound (locale, key) {
        console.warn(`i18n :: Key '${key}' not found for locale '${locale}'`);
    }
});

// add translations directly to the application
Vue.i18n.add('ru', RU);
Vue.i18n.add('ua', UA);

// set the start locale to use
Vue.i18n.set('ru');

Vue.component('v-select', vSelect);
new Vue({
    router,
    store
}).$mount('#app');