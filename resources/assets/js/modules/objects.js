/*
|-------------------------------------------------------------------------------
|-------------------------------------------------------------------------------
| The Vuex data store for the cafes
* status = 0 -> No loading has begun
* status = 1 -> Loading has started
* status = 2 -> Loading completed successfully
* status = 3 -> Loading completed unsuccessfully
*/
import ObjectAPI from '../api/object.js';
import RegionAPI from '../api/region.js';

import Vue from 'vue';

export const objects = {
    state: {
        objects: [],
        selectedObjects: [],
        object: {},
        filters: {
            minArea: 0,
            maxArea: 100,
            regions: [

            ],
            categories: [
                {
                    id: 1, title: 'landUsage.agricultural', isChecked: true
                },
                {
                    id: 2, title: 'landUsage.farmManagement', isChecked: true
                },
            ]
        },
        regions: [
        ],
    },
    getters: {
        getObjects: function(state) {
            let selectedRegionIds = state.filters.regions.map(
                function(item) {
                    return item.region_id;
                }
            );
            return state.objects.filter(function(object) {
                if (object.area > state.filters.minArea &&
                    object.area < state.filters.maxArea &&
                    selectedRegionIds.indexOf(object.region_id) !== -1 &&
                    ((state.filters.categories[1].isChecked && object.category_id === 2) ||
                        (state.filters.categories[0].isChecked && object.category_id === 1))
                ) {
                    return object;
                }
            });
        },

        getObject: function(state)
        {
            return state.object;
        },

        getRegions: function (state) {
            return state.regions.map(function(item) {

                item.label = item.region_ua;
                return item;
            });
        },

        getSelectedRegions: function (state) {
            return state.filters.regions;
        },

        getCategories: function(state) {
            return state.filters.categories;
        }
    },
    actions: {
        loadObjects({commit})
        {
            ObjectAPI.getObjects().then(function(response) {
                commit('setObjects', response.data);
            }).catch(function() {
                commit('setObjects', []);
            });
        },

        loadRegions({commit})
        {
            RegionAPI.getRegions().then(function(response) {
                commit('setRegions', response.data);
            }).catch(function() {
                commit('setRegions', []);
            });
        },

        loadObject({commit}, data)
        {
            ObjectAPI.getObject(data.id).then(function(response) {
                commit('setObject', response.data);
            }).catch(function() {
                commit('setObject', {});
            });
        },

    },
    mutations: {
        setObjects(state, objects)
        {
            state.objects = objects;
        },

        setRegions(state, regions)
        {
            state.regions = regions;
        },

        setObject(state, object)
        {
            state.object = object;
        },

        setMinArea: function (state, data) {
            state.filters.minArea = data;
        },

        setMaxArea: function (state, data) {
            state.filters.maxArea = data;
        },

        setSelectedRegions: function (state, data) {
            state.filters.regions = data;
        }
        ,
        setCategories: function(state, data) {
            state.filters.categories = data;
        }
    },
};