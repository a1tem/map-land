import Vue from 'vue';

export const localization = {
    /*
      Defines the state being monitored for the module.
    */
    state: {
        availableLang: [
            'ru',
            'ua'
        ],
        defaultLang: 'ua'
    },

    /*
      Defines the mutations used
    */
    mutations: {
        setLang: function(state, data){
            if (state.availableLang.indexOf(data) !== -1) {
                Vue.i18n.set(data);
            } else {
                Vue.i18n.set(state.defaultLang);
            }
        },
    },

    /*
      Defines the getters used by the module.
    */
    getters: {
        getLang:function (state){
            return Vue.i18n.locale();
        },
        getAvailableLang: function(state) {
            return state.availableLang;
        },
        getDefaultLang: function(state) {
            return state.defaultLang;
        }
    }
};