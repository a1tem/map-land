/*
|-------------------------------------------------------------------------------
| routes.js
|-------------------------------------------------------------------------------
| Contains all of the routes for the application
*/
/*
Imports Vue and VueRouter to extend with the routes.
*/
import Vue from 'vue';
import VueRouter from 'vue-router';

/*
Extends Vue to use Vue Router
*/
Vue.use(VueRouter);

export default new VueRouter({
    routes: [
        {
            path: '/',
            redirect: '/ua',
            component: Vue.component('App', require( './pages/App.vue' )),
            children: [
                {
                    name: 'App',
                    path: ':lang/',
                    component: Vue.component('App', require( './pages/App.vue' )),
                },
            ]
        },
    ],
});