/*
* Imports the Roast API URL from the config.
*/
import {APP_CONFIG} from '../config.js';

export default {

    getObjects: function() {
        return axios.get(APP_CONFIG.API_URL + '/objects');
    },

    getObject: function(objectId) {
        return axios.get(APP_CONFIG.API_URL + '/object/' + objectId);
    },
};