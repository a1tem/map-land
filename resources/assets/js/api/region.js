/*
* Imports the Roast API URL from the config.
*/
import {APP_CONFIG} from '../config.js';

export default {

    getRegions: function() {
        return axios.get(APP_CONFIG.API_URL + '/regions');
    },
};