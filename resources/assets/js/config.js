/*
* Defines the API route we are using.
*/
var api_url = '';
var google_maps_js_api = 'AIzaSyCp9KYap6Z83PsU9bW_sxJJcPfygnkKQJE';
switch( process.env.NODE_ENV ){
    case 'development':
        api_url = 'http://roast.develop/api/v1';
        break;
    case 'production':
        api_url = 'https://mapland.com.ua/api/v1';
        break;
}

export const APP_CONFIG = {
    API_URL: api_url,
    GOOGLE_MAPS_JS_API: google_maps_js_api
};