/*
|-------------------------------------------------------------------------------
| VUEX store.js
|-------------------------------------------------------------------------------
| Builds the data store from all of the modules for the Roast app.
*/
/*
  Adds the promise polyfill for IE 11
*/
require('es6-promise').polyfill();
/*
    Imports Vue and Vuex
*/
import Vue from 'vue';
import Vuex from 'vuex';
/*
    Imports all of the modules used in the application to build the data store.
*/
import {objects} from './modules/objects';
import {users} from './modules/users';
import {localization} from './modules/localization';
/*
    Initializes Vuex on Vue.
*/
Vue.use(Vuex);

const store = new Vuex.Store({
    modules: {
        objects,
        users,
        localization
    },
});
/*
  Exports our data store.
*/
export {store};