<?php

namespace App\Utilities;

use proj4php\Proj4php;
use proj4php\Proj;
use proj4php\Point;
use Geokit\Math as Math;

class Coordinates
{
    public static function convertCoordinates($xmin, $ymin, $xmax, $ymax)
    {
        // Initialise Proj4
        $proj = new Proj4php();

        // Create two different projections.
        $proj3857 = new Proj('EPSG:3857', $proj);
        $projWGS84 = new Proj('EPSG:4326', $proj);

        // Create a point.
        $pointSrc = new Point($xmin, $ymin, $proj3857);

        // Transform the point between datums.
        $pointDest = $proj->transform($projWGS84, $pointSrc);
        $min = $pointDest->toArray();
        // Create a point.
        $pointSrc = new Point($xmax, $ymax, $proj3857);

        // Transform the point between datums.
        $pointDest = $proj->transform($projWGS84, $pointSrc);
        $max = $pointDest->toArray();
        $math = new Math();

        return $math->midpoint($min, $max);
    }

}