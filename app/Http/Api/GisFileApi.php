<?php

namespace App\Http\Api;

use App\Utilities\Coordinates;

class GisFileApi extends RemoteApi
{
    /**
     * Api url
     */
    const API_URL = 'http://gisfile.com/';

    /**
     * GisFileApi constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /*
      Geocodes an address so we can get the latitude and longitude
    */

    protected static function prepareUrlString($code)
    {
        $attr = explode(":", $code);
        if (!empty($attr) && isset($attr[0]) && isset($attr[1]) && isset($attr[2]) && isset($attr[3])) {
            $result = 'koatuu=' . $attr[0] . '&zone=' . $attr[1] . '&quartal=' . $attr[2] . '&parcel=' . $attr[3];
        } else {
            $result = '';
        }

        return $result;
    }

    public function getCoordinatesByCode($code)
    {
        /*
          Builds the URL and request to the Google Maps API
        */
        $url = self::API_URL . 'layer/cadmap/search?cadnum=' . $code;

        $data = $this->get($url);

        $result = [];

        /*
          If the response is not empty (something returned),
          we extract the latitude and longitude from the
          data.
        */
        if (!empty($data) && isset($data->data) && isset($data->data[0])) {
            $result = Coordinates::convertCoordinates(
                $data->data[0]->st_xmin,
                $data->data[0]->st_ymin,
                $data->data[0]->st_xmax,
                $data->data[0]->st_ymax
            );
        }

        return $result;

    }

    public function getInformationByCode($code)
    {
        $searchString = self::prepareUrlString($code);
        $url = self::API_URL . 'layer/cadmap/search?' . $searchString;

        $data = $this->get($url);

        $result = [];
        if (!empty($data) && isset($data->data) && isset($data->data[0])) {
            $result = $data->data[0];
        }

        return $result;
    }

}