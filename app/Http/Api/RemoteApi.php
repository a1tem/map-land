<?php

namespace App\Http\Api;

/**
 * Class RemoteApi
 * @package App\Http\Api
 */
abstract class RemoteApi
{
    /**
     * @var \GuzzleHttp\Client
     */
    protected $client;

    /**
     * @var array of specified headers
     */
    protected $headers = [];

    /**
     * RemoteApi constructor.
     */
    public function __construct()
    {
        $this->client = new \GuzzleHttp\Client();
    }

    /**
     * Make GET request
     *
     * @param string $url - server url
     * @param array $options - array of available search options
     *
     * @return \Psr\Http\Message\StreamInterface
     */
    public function get($url, $options = array())
    {
        if (!empty($options)) {
            $url = $url . $this->prepareOptions($options);
        }

        $res = $this->client->request('GET', $url, [
            'headers' => $this->headers,
        ]);

        return json_decode($res->getBody());
    }

    /**
     * Make POST request to the server
     *
     * @param $url - server url
     * @param $data - data which will be sent to the server
     *
     * @return \Psr\Http\Message\StreamInterface
     */
    public function post($url, $data) {
        $res = $this->client->request('POST', $url, [
            'headers' => $this->headers,
            'body' => json_encode($data),
        ]);

        return json_decode($res->getBody());
    }

    /**
     * Prepares string of options, which will be merged with url
     *
     * @param array $options - array of options
     * @return string - prepared options string
     */
    protected function prepareOptions(array $options)
    {
        $result = '?';

        foreach ($options as $key => $value) {
            $result .= $key . '=' .$value .'&';
        }

        return rtrim($result, "&");
    }
}