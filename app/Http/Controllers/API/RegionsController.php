<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Region;


class RegionsController extends Controller
{
    /*
    |-------------------------------------------------------------------------------
    | Get All Objects
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/regions
    | Method:         GET
    | Description:    Gets all of the cafes in the application
    */
    public function getRegions()
    {
        $cafes = Region::get();

        return response()->json($cafes);
    }

}