<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Object;

class ObjectsController extends Controller
{
    /*
    |-------------------------------------------------------------------------------
    | Get All Objects
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/objects
    | Method:         GET
    | Description:    Gets all of the cafes in the application
    */
    public function getObjects()
    {
        $cafes = Object::withCoordinates()->get();

        return response()->json($cafes);
    }

    /*
    |-------------------------------------------------------------------------------
    | Get An Individual Object
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/objects/{id}
    | Method:         GET
    | Description:    Gets an individual object
    | Parameters:
    |   $id   -> ID of the object we are retrieving
    */
    public function getObject($id)
    {
        $object = Object::where('id', '=', $id)->first();

        return response()->json($object);
    }

}