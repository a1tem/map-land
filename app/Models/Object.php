<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Object extends Model
{
    protected $table = 'objects';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at','deleted_at',
    ];

    /**
     * Scope a query to only include objects with coordinates.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithCoordinates($query)
    {
        return $query->whereNotNull('lat')
            ->whereNotNull('lng')
            ->whereNotNull('category_id')
            ->where('lat', '!=', 0)
            ->where('lng', '!=', 0);
    }
}